<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register')->name('register');
Route::post('login', 'AuthController@login')->name('login');
Route::get('stores', 'PlaceController@all')->name('stores');
Route::get('store', 'PlaceController@show')->name('store');
Route::post('register-access', 'RegisterAccessController@addRegister')->name('register-access');
Route::get('register-users', 'RegisterAccessController@getRegistersForUsers')->name('register-users');
Route::get('last-user', 'RegisterAccessController@getLastRegisterForUser')->name('last-user');

Route::middleware('auth:api')->group(function() {
    Route::get('user/{userId}/detail', 'UserController@show');
    // Route::get('stores', 'PlaceController@all')->name('stores');
    // Route::get('store', 'PlaceController@show')->name('store');
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
