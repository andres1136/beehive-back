<?php

namespace App\Http\Controllers;

use App\Place;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    public function all() {
       $places = Place::all();

       return response()->json($places);
    }

    public function show(Request $request) {
        $place = Place::find($request->id);

        return response()->json($place);
    }

}
