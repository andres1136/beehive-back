<?php

namespace App\Http\Controllers;

use App\RegisterAccess;
use Illuminate\Http\Request;

class RegisterAccessController extends Controller
{
    public function addRegister(Request $request) {

        $request->validate([
            'user_id' => 'required',
            'place_id' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
        ]);

        RegisterAccess::create([
            'user_id' => $request->user_id,
            'place_id' => $request->place_id,
            'hora_entrada' => $request->date_start,
            'hora_ingreso' => $request->date_end,
            'status' => $request->status,
        ]);

        return response()->json('Usuario agregado correctamente');
    }

    public function getRegistersForUsers(Request $request) {

        $request->validate([
            'id' => 'required',
        ]);

        $registers = RegisterAccess::with('place')->where('user_id', $request->id)->orderBy('hora_entrada', 'DESC')->get();

        return response()->json($registers);
    }


    public function getLastRegisterForUser(Request $request) {

        $request->validate([
            'id' => 'required',
        ]);

        $register = RegisterAccess::where('user_id', $request->id)->orderBy('hora_entrada', 'DESC')->first();

        return response()->json($register);
    }



}
