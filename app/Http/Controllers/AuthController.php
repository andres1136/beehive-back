<?php

namespace App\Http\Controllers;

use App\User;
use Facade\Ignition\QueryRecorder\Query;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'documentType' => 'required|min:2',
            'document' => 'required',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'documentType' =>  $request->documentType,
            'document' =>  $request->document,
        ]);

        $userSend = User::where('id', $user->id)->get();

        return response()->json($userSend);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json('Los datos no coinciden', 422);
        }

        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password]) ) {
            $user = Auth::user();

            $token = $user->createToken($user->email.'-'.now());

            return response()->json([
                'user'  => $user,
                'token' => $token->accessToken
            ]);
        }
    }
}
