<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComercialType extends Model
{
    protected $table = 'comercial_type';

    protected $fillable = ['valor'];
}
