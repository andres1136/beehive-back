<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaceStore extends Model
{
    protected $table = 'store_visited';

    protected $fillable = ['place_id', 'store_id', 'local'];

    public function place(){
        return $this->belongsTo(Place::class);
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }
}
