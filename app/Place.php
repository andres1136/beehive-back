<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'places';

    protected $fillable = ['nombre','direccion','qr', 'latitud', 'longitud','maxima_ocupacion','hora_apertura','hora_cierre', 'url_imagen'];

}
