<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterAccess extends Model
{
    protected $table = 'register_access';

    protected $fillable = ['user_id', 'place_id', 'status' ,'hora_entrada', 'hora_ingreso'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function place(){
        return $this->belongsTo(Place::class);
    }
}
