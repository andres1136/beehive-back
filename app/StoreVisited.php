<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreVisited extends Model
{
    protected $table = 'store_visited';

    protected $fillable = ['place_stores_id', 'register_access_id'];

    public function place(){
        return $this->belongsTo(Place::class);
    }

    public function registerAccess(){
        return $this->belongsTo(RegisterAccess::class);
    }
}
