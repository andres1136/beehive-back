<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'store';

    protected $fillable = ['nombre', 'phone', 'comercial_type_id'];

    public function comercialType(){
        return $this->belongsTo(ComercialType::class);
    }
}
