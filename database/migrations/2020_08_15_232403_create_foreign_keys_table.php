<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store', function (Blueprint $table) {
            $table->foreign('comercial_type_id')->references('id')->on('comercial_type');
        });

        Schema::table('place_stores', function (Blueprint $table) {
            $table->foreign('place_id')->references('id')->on('places');
            $table->foreign('store_id')->references('id')->on('store');
        });

        Schema::table('register_access', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('place_id')->references('id')->on('places');
        });

        Schema::table('store_visited', function (Blueprint $table) {
            $table->foreign('place_stores_id')->references('id')->on('place_stores');
            $table->foreign('register_access_id')->references('id')->on('register_access');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_keys');
    }
}
