<?php

use Illuminate\Database\Seeder;

class StoreTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('store')->delete();
        
        \DB::table('store')->insert(array (
            0 => 
            array (
                'comercial_type_id' => 1,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 1,
                'nombre' => 'Tienda 1',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            1 => 
            array (
                'comercial_type_id' => 2,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 2,
                'nombre' => 'Tienda 2',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            2 => 
            array (
                'comercial_type_id' => 3,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 3,
                'nombre' => 'Tienda 3',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            3 => 
            array (
                'comercial_type_id' => 4,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 4,
                'nombre' => 'Tienda 4',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            4 => 
            array (
                'comercial_type_id' => 1,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 5,
                'nombre' => 'Tienda 5',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            5 => 
            array (
                'comercial_type_id' => 2,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 6,
                'nombre' => 'Tienda 6',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            6 => 
            array (
                'comercial_type_id' => 3,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 7,
                'nombre' => 'Tienda 7',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            7 => 
            array (
                'comercial_type_id' => 4,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 8,
                'nombre' => 'Tienda 8',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            8 => 
            array (
                'comercial_type_id' => 1,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 9,
                'nombre' => 'Tienda 9',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            9 => 
            array (
                'comercial_type_id' => 2,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 10,
                'nombre' => 'Tienda 10',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            10 => 
            array (
                'comercial_type_id' => 3,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 11,
                'nombre' => 'Tienda 11',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            11 => 
            array (
                'comercial_type_id' => 4,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 12,
                'nombre' => 'Tienda 12',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            12 => 
            array (
                'comercial_type_id' => 1,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 13,
                'nombre' => 'Tienda 13',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            13 => 
            array (
                'comercial_type_id' => 2,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 14,
                'nombre' => 'Tienda 14',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            14 => 
            array (
                'comercial_type_id' => 3,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 15,
                'nombre' => 'Tienda 15',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
            15 => 
            array (
                'comercial_type_id' => 4,
                'created_at' => '2020-08-15 19:31:34',
                'id' => 16,
                'nombre' => 'Tienda 16',
                'phone' => 4444444,
                'updated_at' => '2020-08-15 19:31:34',
            ),
        ));
        
        
    }
}