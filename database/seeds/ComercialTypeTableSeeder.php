<?php

use Illuminate\Database\Seeder;

class ComercialTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('comercial_type')->delete();
        
        \DB::table('comercial_type')->insert(array (
            0 => 
            array (
                'created_at' => '2020-08-15 19:29:01',
                'id' => 1,
                'updated_at' => '2020-08-15 19:29:01',
                'valor' => 'Comercial 1',
            ),
            1 => 
            array (
                'created_at' => '2020-08-15 19:29:01',
                'id' => 2,
                'updated_at' => '2020-08-15 19:29:01',
                'valor' => 'Comercial 2',
            ),
            2 => 
            array (
                'created_at' => '2020-08-15 19:29:01',
                'id' => 3,
                'updated_at' => '2020-08-15 19:29:01',
                'valor' => 'Comercial 3',
            ),
            3 => 
            array (
                'created_at' => '2020-08-15 19:29:01',
                'id' => 4,
                'updated_at' => '2020-08-15 19:29:01',
                'valor' => 'Comercial 4',
            ),
        ));
        
        
    }
}