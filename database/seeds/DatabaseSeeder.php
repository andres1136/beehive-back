<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
            'name' => 'Andrés',
            'email' => 'user@email.com',
            'password' => bcrypt('password'),
            'documentType' => 'CC',
            'document' => '1136883231',
        ]);
        $this->call(PlacesTableSeeder::class);
        $this->call(ComercialTypeTableSeeder::class);
        $this->call(StoreTableSeeder::class);
        $this->call(PlaceStoresTableSeeder::class);
    }
}
