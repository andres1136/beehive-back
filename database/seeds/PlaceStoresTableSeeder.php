<?php

use Illuminate\Database\Seeder;

class PlaceStoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('place_stores')->delete();
        
        \DB::table('place_stores')->insert(array (
            0 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 1,
                'local' => 'Local 1',
                'place_id' => 1,
                'store_id' => 1,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            1 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 2,
                'local' => 'Local 2',
                'place_id' => 2,
                'store_id' => 2,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            2 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 3,
                'local' => 'Local 3',
                'place_id' => 3,
                'store_id' => 3,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            3 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 4,
                'local' => 'Local 4',
                'place_id' => 4,
                'store_id' => 4,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            4 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 5,
                'local' => 'Local 1',
                'place_id' => 1,
                'store_id' => 1,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            5 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 6,
                'local' => 'Local 2',
                'place_id' => 2,
                'store_id' => 2,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            6 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 7,
                'local' => 'Local 3',
                'place_id' => 3,
                'store_id' => 3,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            7 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 8,
                'local' => 'Local 4',
                'place_id' => 4,
                'store_id' => 4,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            8 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 9,
                'local' => 'Local 1',
                'place_id' => 1,
                'store_id' => 1,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            9 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 10,
                'local' => 'Local 2',
                'place_id' => 2,
                'store_id' => 2,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            10 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 11,
                'local' => 'Local 3',
                'place_id' => 3,
                'store_id' => 3,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            11 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 12,
                'local' => 'Local 4',
                'place_id' => 4,
                'store_id' => 4,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            12 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 13,
                'local' => 'Local 1',
                'place_id' => 1,
                'store_id' => 1,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            13 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 14,
                'local' => 'Local 2',
                'place_id' => 2,
                'store_id' => 2,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            14 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 15,
                'local' => 'Local 3',
                'place_id' => 3,
                'store_id' => 3,
                'updated_at' => '2020-08-15 19:36:11',
            ),
            15 => 
            array (
                'created_at' => '2020-08-15 19:36:11',
                'id' => 16,
                'local' => 'Local 4',
                'place_id' => 4,
                'store_id' => 4,
                'updated_at' => '2020-08-15 19:36:11',
            ),
        ));
        
        
    }
}