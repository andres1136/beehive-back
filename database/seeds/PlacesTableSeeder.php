<?php

use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('places')->delete();

        \DB::table('places')->insert(array (
            0 =>
            array (
                'created_at' => '2020-08-15 19:23:29',
                'direccion' => 'Calle 38 a Sur # 34d-51',
                'hora_apertura' => '09:00:00',
                'hora_cierre' => '21:00:00',
                'id' => 1,
                'maxima_ocupacion' => 300,
                'latitud' => 4.592214,
                'longitud' => -74.123552,
                'nombre' => 'Centro Comercial Centro Mayor',
                'qr' => NULL,
                'updated_at' => '2020-08-15 19:23:32',
                'url_imagen' => 'https://cdn.pixabay.com/photo/2015/08/25/11/50/shopping-mall-906721_960_720.jpg',
            ),
            1 =>
            array (
                'created_at' => '2020-08-15 19:24:42',
                'direccion' => 'Ac. 26 #62-47',
                'hora_apertura' => '09:00:00',
                'hora_cierre' => '21:00:00',
                'id' => 2,
                'maxima_ocupacion' => 0,
                'latitud' => 4.647836,
                'longitud' => -74.101174,
                'nombre' => 'Centro Comercial Gran Estación',
                'qr' => NULL,
                'updated_at' => '2020-08-15 19:24:44',
                'url_imagen' => 'https://cdn.pixabay.com/photo/2017/07/31/11/43/architecture-2557567_960_720.jpg',
            ),
            2 =>
            array (
                'created_at' => '2020-08-15 19:26:08',
                'direccion' => 'Av. Boyacá #80-94',
                'hora_apertura' => '09:00:00',
                'hora_cierre' => '21:00:00',
                'id' => 3,
                'maxima_ocupacion' => 300,
                'latitud' => 4.694965,
                'longitud' => -74.085534,
                'nombre' => 'Centro comercial Titán Plaza',
                'qr' => NULL,
                'updated_at' => '2020-08-15 19:26:10',
                'url_imagen' => 'https://cdn.pixabay.com/photo/2015/08/25/11/49/stairs-906720_960_720.jpg',
            ),
            3 =>
            array (
                'created_at' => '2020-08-15 19:27:18',
                'direccion' => 'Cra. 71d #6-94',
                'hora_apertura' => '09:00:00',
                'hora_cierre' => '21:00:00',
                'id' => 4,
                'maxima_ocupacion' => 250,
                'latitud' => 4.618726,
                'longitud' => -74.134512,
                'nombre' => 'Centro Comercial Plaza de las Americas',
                'qr' => NULL,
                'updated_at' => '2020-08-15 19:27:25',
                'url_imagen' => '"https://cdn.pixabay.com/photo/2014/10/22/18/24/central-embassy-498560_960_720.jpg',
            ),
        ));


    }
}
